#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

void fun(char** str_ref)
{
    str_ref++;
}
 
int main()
{
    char *str = (void *)malloc(100*sizeof(char));
    strcpy(str, "Merilline - Automated your shipping");
    fun(&str);
    puts(str);
    free(str);
    return 0;
}